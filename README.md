# friendlycaptchakt

An implementation of [FriendlyCaptcha's](https://friendlycaptcha.com/) [friendly-pow](https://github.com/FriendlyCaptcha/friendly-pow) project which is the backing code for their captcha solution. This project implements all required methods to generate and validate captchas which can be used by [friendly-challenge](https://github.com/FriendlyCaptcha/friendly-challenge), and is intended for use in backend software written in Kotlin/Java/other JVM languages. For more information, please refer to their [self-hosting guide](https://github.com/FriendlyCaptcha/friendly-challenge/blob/master/docs/self_hosting.md).

## Usable via
[![](https://jitpack.io/v/com.gitlab.grrfe/friendlycaptchakt.svg)](https://jitpack.io/#com.gitlab.grrfe/friendlycaptchakt)

## Examples

coming soon TM
