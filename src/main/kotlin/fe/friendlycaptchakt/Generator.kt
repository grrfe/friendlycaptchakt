package fe.friendlycaptchakt

object Generator {
    const val PUZZLE_TIMESTAMP_OFFSET = 0
    const val ACCOUNT_ID_OFFSET = 4
    const val APP_ID_OFFSET = 8
    const val PUZZLE_VERSION_OFFSET = 12
    const val PUZZLE_EXPIRY_OFFSET = 13
    const val NUMBER_OF_PUZZLES_OFFSET = 14
    const val PUZZLE_DIFFICULTY_OFFSET = 15
    const val PUZZLE_NONCE_OFFSET = 24
    const val PUZZLE_USER_DATA_OFFSET = 32
    const val PUZZLE_USER_DATA_MAX_LENGTH = 32

    const val VERSION = 1
    const val HASH_SIZE_BYTES = 32


    @ExperimentalUnsignedTypes
    fun generatePuzzleBuffer(
        timeInSec: Long = System.currentTimeMillis() / 1000,
        accountId: Int = 0,
        appId: Int = 0,
        puzzleExpiry: Byte = 10,
        puzzleAmount: Byte = 1,
        difficulty: Int = 10,
        userData: IntArray? = null,
        nonce: ByteArray,
    ): UByteArray {
        val bytes = HASH_SIZE_BYTES
        val puzzle = UByteArray(bytes)

        val timeSec = timeInSec.toUInt().toUByteArray(IntEndian.BIG)

        puzzle.set(PUZZLE_TIMESTAMP_OFFSET, timeSec)
        puzzle.set(ACCOUNT_ID_OFFSET, accountId.toUByteArray(IntEndian.BIG))
        puzzle.set(APP_ID_OFFSET, appId.toUByteArray(IntEndian.BIG))
        puzzle[PUZZLE_VERSION_OFFSET] = VERSION.toUByte()
        puzzle[PUZZLE_EXPIRY_OFFSET] = puzzleExpiry.toUByte()
        puzzle[NUMBER_OF_PUZZLES_OFFSET] = puzzleAmount.toUByte()
        puzzle[PUZZLE_DIFFICULTY_OFFSET] = difficulty.toUByte()

        puzzle.set(PUZZLE_NONCE_OFFSET, nonce.toUByteArray())

        userData?.let { data ->
            puzzle.set(PUZZLE_USER_DATA_OFFSET, data.copyOfRange(0, PUZZLE_USER_DATA_MAX_LENGTH))
        }

        return puzzle
    }
}