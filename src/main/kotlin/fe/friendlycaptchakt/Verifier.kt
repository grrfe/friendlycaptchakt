package fe.friendlycaptchakt

import fe.friendlycaptchakt.Generator.HASH_SIZE_BYTES
import fe.friendlycaptchakt.Generator.NUMBER_OF_PUZZLES_OFFSET
import fe.friendlycaptchakt.Generator.PUZZLE_DIFFICULTY_OFFSET
import fe.friendlycaptchakt.Generator.PUZZLE_EXPIRY_OFFSET
import fe.friendlycaptchakt.Generator.PUZZLE_TIMESTAMP_OFFSET
import org.kocakosm.jblake2.Blake2b
import kotlin.math.pow

object Verifier {
    @ExperimentalUnsignedTypes
    fun checkPuzzleExpiry(puzzleBuffer: UByteArray): Boolean {
        val time = System.currentTimeMillis() / 1000

        val puzzleTimestamp = puzzleBuffer.getUInt(PUZZLE_TIMESTAMP_OFFSET, IntEndian.BIG)
        val expiryByte = puzzleBuffer[PUZZLE_EXPIRY_OFFSET]

        return puzzleTimestamp + expiryToDurationInSeconds(expiryByte) >= time.toUInt()
    }

    @ExperimentalUnsignedTypes
    fun expiryToDurationInSeconds(value: UByte) = capValue(value) * 300u

    @ExperimentalUnsignedTypes
    private fun capValue(value: UByte) = maxOf(minOf(value, 255u), 0u)

    @ExperimentalUnsignedTypes
    fun difficultyToThreshold(value: UByte): UInt {
        return (2.0.pow((255.999 - (capValue(value).toDouble())) / 8.0).toUInt() shr 0)
    }

    @ExperimentalUnsignedTypes
    fun checkAmountOfSolutions(puzzleBuffer: UByteArray, solutionBuffer: UByteArray): Boolean {
        return solutionBuffer.size == puzzleBuffer[NUMBER_OF_PUZZLES_OFFSET].toInt() * 8
    }

    const val CHALLENGE_SIZE_BYTES = 128

    @ExperimentalUnsignedTypes
    fun checkSolutions(puzzleBuffer: UByteArray, solutionBuffer: UByteArray): Boolean {
        val threshold = difficultyToThreshold(puzzleBuffer[PUZZLE_DIFFICULTY_OFFSET])
        val blake = Blake2b(HASH_SIZE_BYTES)

        val input = UByteArray(CHALLENGE_SIZE_BYTES)
        input.set(0, puzzleBuffer)

        val solutions = mutableSetOf<String>()
        (0 until solutionBuffer.size / 8).forEach { i ->
            val solution = solutionBuffer.copyOfRange(i * 8, (i * 8) + 8)
            val solutionString = solution.toString()
            if (solutions.contains(solutionString)) {
                return false
            }

            solutions.add(solutionString)

            input.set(120, solution)

            input.toByteArray().also {
                blake.update(it, 0, it.size)
            }

            if (blake.digest().toUByteArray().getUInt(0, IntEndian.LITTLE) >= threshold) {
                return false
            }
        }

        return true
    }
}