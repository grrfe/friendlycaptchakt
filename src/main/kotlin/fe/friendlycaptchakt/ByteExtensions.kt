package fe.friendlycaptchakt

enum class IntEndian(val shiftFrom: Int, val shiftStep: Int) {
    LITTLE(0, 8), BIG(24, -8);
}


@ExperimentalUnsignedTypes
fun UByteArray.getUInt(index: Int, endian: IntEndian): UInt {
    val shift = endian.shiftFrom
    var result = this[index].toUInt() shl shift
    for (i in index + 1 until index + 4) {
        result = result or (this[i].toUInt() shl (shift + endian.shiftStep * i))
    }

    return result
}

@ExperimentalUnsignedTypes
fun UByteArray.set(index: Int, other: UByteArray) {
    other.forEachIndexed { i, uByte ->
        this[index + i] = uByte
    }
}

@ExperimentalUnsignedTypes
fun UByteArray.set(index: Int, other: IntArray) {
    other.forEachIndexed { i, int ->
        this[index + i] = int.toUByte()
    }
}


@ExperimentalUnsignedTypes
fun UInt.toUByteArray(endian: IntEndian) = UByteArray(4) { i ->
    (this shr endian.shiftFrom + endian.shiftStep * i).toUByte()
}

@ExperimentalUnsignedTypes
fun Int.toUByteArray(endian: IntEndian) = this.toUInt().toUByteArray(endian)
