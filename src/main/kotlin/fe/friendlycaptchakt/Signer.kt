package fe.friendlycaptchakt

import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

object Signer {
    private const val HMAC_256 = "HmacSHA256"

    fun checkSignature(base64PuzzleBuffer: String, signature: String, signingSecret: String): Boolean {
        return signature == generatePuzzleBufferSignature(base64PuzzleBuffer, signingSecret)
    }

    fun generatePuzzleBufferSignature(base64PuzzleBuffer: String, signingSecret: String): String {
        val mac = Mac.getInstance(HMAC_256).apply {
            this.init(SecretKeySpec(signingSecret.toByteArray(), HMAC_256))
        }

        return mac.doFinal(base64PuzzleBuffer.toByteArray()).toHex().take(32)
    }

    private fun ByteArray.toHex(): String {
        val array = this
        return buildString {
            array.forEach { b ->
                append("%02x".format(b))
            }
        }
    }

}